<?php

use App\Http\Controllers\RegistrationController;
use App\Registration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');

Route::post('signup', 'LeadController@store');

Route::get('session', function() {
    return response()->json(['user' => Auth::user()]);
});

Route::get('register/{registration:token}', 'RegistrationController@show');
Route::patch('register/{registration:token}', 'RegistrationController@update');

// Sanctum protected API routes
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

