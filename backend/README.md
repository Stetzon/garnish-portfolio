# Garnish Health API

## Install Instructions
1. Optionally fork then clone repo:

    `git clone git@github.com:<username>/garnish-mvp.git`
2. Install PHP dependencies: 

    `composer install`
3. Install JavaScript dependencies: 

    `npm install` 
    
    or 
    
    `yarn install`
    
4. Update env
  ```
  cp .env.example .env
  php artisan key:generate
  ```

  then update `.env` with appropriate credentials
  
5. Run migrations
  ```
  php artisan migrate
  ```

  Note: Mysql user must use `caching_sha2_password` for authentication for Mysql v8 and Php7.4 otherwise Laravel cannot connect to DB.  See instructrions [here](https://php.watch/articles/PHP-7.4-MySQL-8-server-gone-away-fix) to update

6. Run

    `php artisan serve` 

    or if on OSX use [Valet](https://laravel.com/docs/7.x/valet)
