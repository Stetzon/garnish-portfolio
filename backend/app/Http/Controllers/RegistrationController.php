<?php

namespace App\Http\Controllers;

use App\Http\Requests\Registrations\UpdateRegistrationRequest;
use App\Registration;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;

class RegistrationController extends Controller
{
        
    /**
     * Return the registration lead and verify registration (email)
     *
     * @param  Registration $registration
     * @return JsonResponse
     */
    public function show(Registration $registration)
    {
        $registration->verified_at = Carbon::now();
        $registration->save();

        $registration->load('lead');

        return response()->json($registration);
    }
  
    /**
     * Update the registration details
     *
     * @param  UpdateRegistrationRequest $request
     * @param  Registration $registration
     * @return void
     */
    public function update(UpdateRegistrationRequest $request, Registration $registration)
    {
        $registration->details = $request->all();
        $registration->save();

        return response()->json(['message' => 'successfully updated registration']);
    }
}
