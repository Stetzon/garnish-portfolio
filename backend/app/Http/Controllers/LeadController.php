<?php

namespace App\Http\Controllers;

use App\Http\Requests\Leads\LeadStoreRequest;
use App\Jobs\Leads\GetLeadScore;
use App\Jobs\Leads\SendWelcomeEmail;
use App\Lead;

class LeadController extends Controller
{
    /**
     * Create a new Lead
     *
     * @param LeadStoreRequest $request
     * @return JsonResponse
     */
    public function store(LeadStoreRequest $request)
    {
        $lead = Lead::create($request->only('email'));

        GetLeadScore::withChain([
            (new SendWelcomeEmail($lead->refresh())),
        ])
            ->dispatch($lead, $request->input('token'));

        return $lead;
    }
}
