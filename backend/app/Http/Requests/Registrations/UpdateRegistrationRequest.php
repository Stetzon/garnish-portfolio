<?php

namespace App\Http\Requests\Registrations;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:100',
            'last_name' => 'required|string|max:100',
            'zip' => 'sometimes|regex:/\b\d{5}\b/',
            'dependents' => 'sometimes|integer|min:0|max:7',
            'coverage' => 'sometimes|integer|in:0,5000,20000,100000,250000,500000',
        ];
    }
}
