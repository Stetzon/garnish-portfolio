<?php

namespace App\Http\Requests\Leads;

use App\Lead;
use Illuminate\Foundation\Http\FormRequest;

class LeadStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:leads',
            'token' => 'required|string',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.email' => "The email <strong>:input</strong> doesn't appear to be valid.",
            'email.unique'  => 'Looks like the email <strong>:input</strong> is already signed up!',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $failedRules = $validator->failed();
            if(!empty($failedRules)) {
                // update the Lead updated timestamp to let us know there was another attempt
                if(isset($failedRules['email']['Unique'])) {
                    $lead = Lead::where(['email' => $this->email])->first();
                    $lead->touch();
                }
            }
        });
    }
}
