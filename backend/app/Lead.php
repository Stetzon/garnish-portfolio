<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::created(function ($lead) {
            Registration::create([
                'lead_id' => $lead->id,
                'token' => md5(config('app.key').$lead->id),
            ]);
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email'
    ];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'email',
    ];

    /**
     * Get the token record associated with the lead.
     */
    public function registration()
    {
        return $this->hasOne(Registration::class);
    }
}
