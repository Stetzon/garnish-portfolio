<?php

namespace App\Jobs\Leads;

use App\Lead;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;

class GetLeadScore implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $lead;
    protected $token;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Lead $lead, string $token)
    {
        $this->lead = $lead;
        $this->token = $token;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $response = Http::asForm()->post(config('captcha.verify_url'), [
            'secret' => config('captcha.secret'),
            'response' => $this->token,
        ]);

        if(!$response->successful()) {
            $this->fail(new Exception($response->status()));
            return;
        }

        $response = $response->json();

        if($response['success'] !== true) {
            $this->fail(new Exception($response['error-codes'][0]));
            return;
        }

        $this->lead->captcha_score = $response['score'];
        $this->lead->save();
    }
}
