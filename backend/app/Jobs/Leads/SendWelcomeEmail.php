<?php

namespace App\Jobs\Leads;

use App\Lead;
use App\Mail\Welcome;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendWelcomeEmail implements ShouldQueue
{
    use Queueable, SerializesModels, InteractsWithQueue;

    protected $lead;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Lead $lead)
    {
        $this->lead = $lead;
    }

    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle()
    {
        if($this->lead->captcha_score < config('captcha.threshold')) {
            $this->delete();
            return;
        }
        Mail::to($this->lead)->send(new Welcome($this->lead));
    }

}
