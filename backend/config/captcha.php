<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Google reCAPTCHA v3 configuration
    |--------------------------------------------------------------------------
    |
    | Documentation: https://developers.google.com/recaptcha/docs/v3
    |
    */

    'secret' => env('CAPTCHA_SECRET_KEY'),
    'verify_url' => env('CAPTCHA_SITE_VERIFY_URL', 'https://www.google.com/recaptcha/api/siteverify'),

    // the minimum score (0-1) threshold in order to pass validation
    'threshold' => env('CAPTCHA_THRESHOLD', 0.7),
];
