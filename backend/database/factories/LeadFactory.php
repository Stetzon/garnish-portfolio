<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Lead;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Lead::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'captcha_score' => $faker->optional(.9)->randomFloat(2, 0, 1),
    ];
});
