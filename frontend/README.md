# frontend

## Project setup
```
npm install
```
Note: will throw error unless font awesome pro registry is configured in npm

Have to set a local environment variable on machine in order for npm to read `.npmrc` file correctly

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
