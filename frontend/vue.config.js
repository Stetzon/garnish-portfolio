module.exports = {
  lintOnSave: 'warning',
  devServer: {
    proxy: 'http://localhost:8000',
  },
  css: {
    loaderOptions: {
      scss: {
        prependData: '@import "@/styles/variables"; @import "@/styles/lib/variables";',
      },
    },
    sourceMap: true,
  },
  productionSourceMap: false,
  chainWebpack: (config) => {
    config
      .plugin('html')
      .tap((args) => {
        // eslint-disable-next-line
        args[0].title = 'Garnish Health';
        return args;
      });
  },
};
