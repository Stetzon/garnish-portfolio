import { library } from '@fortawesome/fontawesome-svg-core';

import {
  faArrowRight,
  faCalendarExclamation,
  faChalkboardTeacher,
  faBell,
  faEye,
  faEyeSlash,
  faExclamationCircle,
  faHandsHelping,
  faHandHoldingMedical,
  faUser,
  faSignOut,
  faSpinner,
  faFileUser,
  faMoneyBill,
  faFilesMedical,
  faCaretCircleRight,
  faEnvelope,
  faEnvelopeSquare,
  faEnvelopeOpenDollar,
  faLock,
  faShareSquare,
  faCommentAltLines,
} from '@fortawesome/pro-regular-svg-icons';

import {
  faFacebookSquare,
  faTwitterSquare,
  faRedditSquare,
  faLinkedin,
} from '@fortawesome/free-brands-svg-icons';

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(faArrowRight,
  faCalendarExclamation,
  faChalkboardTeacher,
  faBell,
  faEye,
  faEyeSlash,
  faExclamationCircle,
  faHandsHelping,
  faHandHoldingMedical,
  faUser,
  faSignOut,
  faSpinner,
  faFileUser,
  faMoneyBill,
  faFilesMedical,
  faCaretCircleRight,
  faEnvelope,
  faEnvelopeOpenDollar,
  faLock,
  faShareSquare,
  faEnvelopeSquare,
  faCommentAltLines,
  faRedditSquare,
  faLinkedin,
  faFacebookSquare,
  faTwitterSquare);

export default FontAwesomeIcon;
