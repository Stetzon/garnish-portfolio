import axios from 'axios';

axios.defaults.withCredentials = true;

if (process.env.NODE_ENV === 'production') {
  axios.defaults.baseURL = process.env.VUE_APP_API_HOST;
}

export default {
  getCSRFCookie() {
    return axios.get('/sanctum/csrf-cookie');
  },
  login(email, password) {
    return axios.post('/api/login', { email, password });
  },
  logout() {
    return axios.post('/api/logout');
  },
  getAuthenticatedUser() {
    return axios.get('/api/session');
  },
  signup(email, token) {
    return axios.post('/api/signup', { email, token });
  },
  getRegistration(token) {
    return axios.get(`/api/register/${token}`);
  },
  updateRegistration(token, details) {
    return axios.patch(`/api/register/${token}`, details);
  },
};
