import Vue from 'vue';
import VueRouter from 'vue-router';
import { HomeHero } from '@/views/landing/sections';

import store from '@/store';
import { Dashboard } from '@/views/dashboard/pages';
import {
  Index,
  Home,
  Login,
  Error as ErrorPage,
} from '../views/landing/pages';

// async loaded components
const Register = () => import('../views/landing/pages/Register.vue');
const PrivacyPolicy = () => import(/* webpackChunkName: "info" */ '../views/landing/pages/PrivacyPolicy.vue');
const Terms = () => import(/* webpackChunkName: "info" */ '../views/landing/pages/Terms.vue');
const FAQs = () => import(/* webpackChunkName: "info" */ '../views/landing/pages/FAQs.vue');

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    component: Index,
    beforeEnter: async (to, from, next) => {
      if (!store.state.cookies.csrf) {
        store.dispatch('getCSRFCookie');
      }
      if (store.state.user.authenticated === null) {
        // store.dispatch('getAuthUser');  disabled for now
        next();
      } else {
        next();
      }
    },
    children: [
      {
        path: '',
        name: 'Home',
        components: {
          default: Home,
          hero: HomeHero,
        },
      },
      {
        path: 'login',
        name: 'Login',
        components: {
          hero: Login,
        },
        beforeEnter: async (to, from, next) => {
          if (store.state.user.authenticated) {
            next({ name: 'Dashboard' });
          } else {
            next();
          }
        },
      },
      {
        path: 'privacy-policy',
        name: 'PrivacyPolicy',
        components: {
          default: PrivacyPolicy,
        },
      },
      {
        path: 'terms-and-conditions',
        name: 'Terms',
        components: {
          default: Terms,
        },
      },
      {
        path: 'faqs',
        name: 'FAQs',
        components: {
          default: FAQs,
        },
      },
      {
        path: 'register/:token',
        name: 'Register',
        components: {
          default: Register,
        },
      },
    ],
  },
  {
    path: '/dashboard',
    component: () => import(/* webpackChunkName: "dashboard" */ '../views/dashboard/pages/Index.vue'),
    beforeEnter: async (to, from, next) => {
      if (store.state.user.authenticated === null) {
        await store.dispatch('getAuthUser');
        if (store.state.user.authenticated) {
          next();
        } else {
          next({ name: 'Login' });
        }
      } else {
        next();
      }
    },
    children: [
      {
        path: '',
        name: 'Dashboard',
        component: Dashboard,
      },
    ],
  },
  {
    path: '*',
    component: ErrorPage,
    props: {
      code: 404,
      message: 'Not Found',
    },
  },
];

const scrollBehavior = (to, from, savedPosition) => {
  switch (to.name) {
    case 'FAQs':
    case 'Terms':
    case 'PrivacyPolicy':
      return { x: 0, y: 0 };
    default:
      break;
  }
  return savedPosition;
};

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior,
});

export default router;
