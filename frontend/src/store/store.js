import Vue from 'vue';
import Vuex from 'vuex';
import 'es6-promise/auto';

import { getToken as getCaptchaToken } from '@/utils/captcha';
import * as ErrrorUtils from '@/utils/errors';
import API from '../lib/api';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: {
      authenticating: false,
      authenticated: null,
      data: null,
    },
    signup: {
      email: null,
      requesting: false,
      signedUp: false,
      error: null,
    },
    cookies: {
      necessary: localStorage.getItem('cookiesAcknowledged'),
      csrf: false,
    },
  },
  mutations: {
    AUTHENTICATING(state) {
      state.user.authenticating = true;
      state.user.authenticated = null;
      state.user.data = null;
    },
    AUTHENTICATED(state, authenticated) {
      state.user.authenticating = false;
      state.user.authenticated = authenticated;
    },
    LOGIN(state, user) {
      state.user.data = { ...state.user.data, ...user };
      state.user.authenticated = true;
      state.user.authenticating = false;
    },
    LOGOUT(state) {
      state.user.authenticated = false;
      state.user.data = null;
    },
    SIGNING_UP(state, signingUp = true) {
      state.signup.requesting = signingUp;
    },
    SIGNED_UP(state, email) {
      state.signup.email = email;
      state.signup.requesting = false;
      state.signup.signedUp = true;
    },
    CLEAR_SIGNUP(state) {
      state.signup.email = null;
      state.signup.signedUp = false;
      state.signup.error = null;
    },
    SIGNUP_ERROR(state, errorMsg) {
      state.signup.error = errorMsg;
    },
    COOKIE_ACKNOWLEDGE(state) {
      localStorage.setItem('cookiesAcknowledged', true);
      state.cookies.necessary = true;
    },
    CSRF_SET(state) {
      state.cookies.csrf = true;
    },
  },
  getters: {
    userAuthenticated: (state) => state.user.authenticated,
    userSignedUp: (state) => state.signup.email !== null,
    cookiesAcknowledged: (state) => state.cookies.necessary === 'true',
  },
  actions: {
    async login({ commit }, { email, password }) {
      commit('AUTHENTICATING');
      await API.login(email, password);

      const { data } = await API.getAuthenticatedUser();
      commit('LOGIN', data.user);

      return 'Login success.';
    },
    async logout({ commit }) {
      await API.logout();

      commit('LOGOUT');
    },
    async getAuthUser({ commit }) {
      commit('AUTHENTICATING');

      try {
        const { data } = await API.getAuthenticatedUser();
        if (data.user) commit('LOGIN', data.user);
        else commit('AUTHENTICATED', false);
      } catch (e) {
        commit('AUTHENTICATED', false);
      }
    },
    async signup({ commit }, email) {
      commit('SIGNING_UP');

      try {
        const token = await getCaptchaToken('signup');
        await API.signup(email, token);
        commit('SIGNED_UP', email);
        window.gtag('event', 'generate_lead', {
          event_label: email,
        });
      } catch (error) {
        if (ErrrorUtils.isValidationError(error)) {
          const errorMsg = ErrrorUtils.getValidationErrors(error)[0];
          commit('SIGNUP_ERROR', errorMsg);
        } else {
          commit('SIGNUP_ERROR', "Oops! We're sorry. Something went wrong.  Please try again later.");
        }
      } finally {
        commit('SIGNING_UP', false);
      }
    },
    clearSignupEmail({ commit }) {
      commit('CLEAR_SIGNUP');
    },
    acknowledgeCookies({ commit }) {
      commit('COOKIE_ACKNOWLEDGE');
    },
    async getCSRFCookie({ commit }) {
      await API.getCSRFCookie();
      commit('CSRF_SET');
    },
  },
});
