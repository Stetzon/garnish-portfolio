import { ToastProgrammatic as Toast } from 'buefy';

export default {
  methods: {
    async logout() {
      await this.$store.dispatch('logout');
      if (this.$route.name !== 'Home') {
        this.$router.replace({
          name: 'Home',
        });
      }
      Toast.open({
        message: 'Successfully signed out!',
        position: 'is-top',
      });
    },
  },
};
