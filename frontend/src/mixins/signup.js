import { mapState } from 'vuex';

export default {
  computed: mapState(['signup']),
  methods: {
    submit() {
      if (this.signup.email) this.$store.dispatch('signup', this.signup.email);
    },
    resetEmail() {
      this.$store.dispatch('clearSignupEmail');
    },
  },
};
