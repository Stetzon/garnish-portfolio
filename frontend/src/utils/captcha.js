const addScript = () => {
  if (document.getElementById('recaptcha')) return;

  const recaptchaScript = document.createElement('script');
  recaptchaScript.async = true;
  recaptchaScript.defer = true;
  recaptchaScript.setAttribute(
    'src',
    `https://www.google.com/recaptcha/api.js?render=${process.env.VUE_APP_CAPTCHA_SITE_KEY}`,
  );
  recaptchaScript.setAttribute('id', 'recaptcha');
  document.head.appendChild(recaptchaScript);
};

// eslint-disable-next-line
const getToken = (actionName) => {
  return window.grecaptcha.execute(process.env.VUE_APP_CAPTCHA_SITE_KEY, { action: actionName });
};

export {
  addScript,
  getToken,
};
