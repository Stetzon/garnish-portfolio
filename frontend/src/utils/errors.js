// Axios error handling documentation: https://github.com/axios/axios#handling-errors

const isValidationError = (error) => {
  if (error.response) {
    return error.response.status === 422;
  }
  return false;
};

const getValidationErrors = (error) => {
  let errors = Object.values(error.response.data.errors);
  errors = errors.flat();
  return errors;
};

export {
  isValidationError,
  getValidationErrors,
};
