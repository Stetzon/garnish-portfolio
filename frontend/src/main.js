import Vue from 'vue';

import {
  Field,
  Input,
  Select,
  Navbar,
  Button,
  Snackbar,
  Toast,
  ConfigProgrammatic,
} from 'buefy';
import AOS from 'aos';
import SocialSharing from 'vue-social-sharing';
import 'aos/dist/aos.css';

import App from './views/App.vue';
import router from './router';
import store from './store';

import './styles/_index.scss';

import FontAwesome from './lib/font-awesome';

AOS.init();

Vue.component('font-awesome-icon', FontAwesome);

Vue.use(SocialSharing);
Vue.use(Field);
Vue.use(Input);
Vue.use(Select);
Vue.use(Navbar);
Vue.use(Button);
Vue.use(Snackbar);
Vue.use(Toast);
ConfigProgrammatic.setOptions({
  defaultIconPack: 'far',
  defaultIconComponent: FontAwesome,
});
Vue.use(ConfigProgrammatic);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
