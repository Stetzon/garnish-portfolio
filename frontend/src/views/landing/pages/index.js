import Index from './Index.vue';
import Home from './Home.vue';
import Login from './Login.vue';
import PrivacyPolicy from './PrivacyPolicy.vue';
import Terms from './Terms.vue';
import FAQs from './FAQs.vue';
import Register from './Register.vue';
import Error from './Error.vue';

export {
  Index,
  Home,
  Login,
  PrivacyPolicy,
  Terms,
  FAQs,
  Register,
  Error,
};
