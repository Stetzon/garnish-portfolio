import About from './About.vue';
import HowItWorks from './HowItWorks.vue';
import Pricing from './Pricing.vue';
import Signup from './Signup.vue';
import Footer from './Footer.vue';
import HomeHero from './HomeHero.vue';


export {
  About,
  HowItWorks,
  Pricing,
  Signup,
  Footer,
  HomeHero,
};
