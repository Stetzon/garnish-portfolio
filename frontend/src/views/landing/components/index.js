import Hero from './Hero.vue';
import TopNav from './TopNav.vue';
import BottomNav from './BottomNav.vue';
import SocialShare from './SocialShare.vue';
import SignupBox from './SignupBox.vue';

export {
  Hero,
  TopNav,
  BottomNav,
  SocialShare,
  SignupBox,
};
